WITH res(qty, date) as
(
SELECT COUNT(DISTINCT P.trip_no) count, date
FROM Pass_in_trip P
JOIN Trip T ON T.trip_no = P.trip_no AND town_from = 'Rostov'
GROUP BY date
)
Select * from res where qty in (select MAX(qty) from res)