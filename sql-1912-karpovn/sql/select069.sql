WITH w as (select point, date, Inc as sm FROM Income
union all
SELECT point, date, -`Out` FROM Outcome)
select point, convert(char(10),date, 1) date,
(SELECT SUM(sm) FROM w
WHERE w.date<=o1.date and w.point=o1.point) as rem
FROM w as o1
GROUP BY point, date