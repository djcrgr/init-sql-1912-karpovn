SELECT q_name
FROM utq
WHERE q_id IN (SELECT DISTINCT b.b_q_id
               FROM (SELECT b_q_id
                     FROM utb
                     GROUP BY b_q_id
                     HAVING SUM(b_vol) = 255 * 3) AS b
               WHERE b.b_q_id NOT IN (
                   SELECT b_q_id
                   FROM utb
                   WHERE b_v_id IN (
                       SELECT b_v_id
                       FROM utb
                       GROUP BY b_v_id
                       HAVING SUM(b_vol) < 255)));