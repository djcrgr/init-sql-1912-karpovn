SELECT c.country, c.class
FROM classes c
WHERE c.country = 'Russia'
UNION
SELECT country, class
FROM classes
WHERE NOT exists(SELECT c.country, c.class
                 FROM classes c
                 WHERE c.country = 'Russia')
