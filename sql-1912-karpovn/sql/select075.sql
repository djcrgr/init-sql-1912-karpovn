SELECT first.maker, laptop, pc, printer
FROM (SELECT maker, COALESCE(MAX(price), NULL) AS laptop
      FROM product
               LEFT JOIN laptop ON (laptop.model = product.model)
      GROUP BY maker) first
         JOIN
     (SELECT maker, COALESCE(MAX(price), NULL) AS pc
      FROM product
               LEFT JOIN pc ON (pc.model = product.model)
      GROUP BY maker) second
     ON (first.maker = second.maker)
         JOIN (SELECT maker, COALESCE(MAX(price), NULL) AS printer
               FROM product
                        LEFT JOIN printer ON (printer.model = product.model)
               GROUP BY maker) third
              ON (third.maker = first.maker)
WHERE first.maker IN
      (SELECT DISTINCT maker
       FROM product
                JOIN pc p ON product.model = p.model
       WHERE p.price IS NOT NULL
       UNION
       SELECT DISTINCT maker
       FROM product
                JOIN laptop l ON product.model = l.model
       WHERE l.price IS NOT NULL
       UNION
       SELECT DISTINCT maker
       FROM product
                JOIN printer p2 ON product.model = p2.model
       WHERE p2.price IS NOT NULL
      )