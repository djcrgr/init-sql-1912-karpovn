SELECT product.* from product
WHERE model NOT IN (
    SELECT *
    FROM (
             SELECT model
             FROM product
             ORDER BY model ASC
             LIMIT 3
         ) AS tmp1
)
  AND model NOT IN (
    SELECT *
    FROM (
             SELECT model
             FROM product
             ORDER BY model DESC
             LIMIT 3
         ) AS tmp2
)