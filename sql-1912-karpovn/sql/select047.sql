WITH t1 AS (SELECT COUNT(name) AS co, country
            FROM (SELECT name, country
                  FROM classes
                           JOIN ships ON ships.class = classes.class
                  UNION
                  SELECT ship, country
                  FROM classes
                           JOIN outcomes ON outcomes.ship = classes.class) f1
            GROUP BY country
),

     t2 AS (SELECT COUNT(name) AS co, country
            FROM (SELECT name, country
                  FROM classes
                           JOIN ships ON ships.class = classes.class
                  WHERE name IN (SELECT DISTINCT ship FROM outcomes WHERE result LIKE 'sunk')
                  UNION
                  SELECT ship, country
                  FROM classes
                           JOIN outcomes ON outcomes.ship = classes.class
                  WHERE ship IN (SELECT DISTINCT ship FROM outcomes WHERE result ='sunk')
                 ) f2
            GROUP BY country)

SELECT t1.country
FROM t1
         JOIN t2 ON t1.co = t2.co AND t1.country = t2.country