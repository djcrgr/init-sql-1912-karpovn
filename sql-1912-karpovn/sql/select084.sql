SELECT c.name, q.dec1, q.dec2, q.dec3
FROM (SELECT w.id_comp,
             SUM(if(DAY(p.date) < 11, 1, 0))                        AS dec1,
             SUM(if((DAY(p.date) > 10 AND DAY(p.date) < 21), 1, 0)) AS dec2,
             SUM(if(DAY(p.date) > 20, 1, 0))                        AS dec3
      FROM trip AS w
               JOIN
           pass_in_trip AS p ON w.trip_no = p.trip_no AND DATE_FORMAT(date, '%Y%m') = '200304'
      GROUP BY w.id_comp
     ) AS q
         JOIN
     company AS c ON q.id_comp = c.id_comp;