SELECT name
FROM ships
         JOIN classes c ON ships.class = c.class
WHERE if(numguns = 8, 1, 0) +
      if(bore = 15, 1, 0) +
      if(displacement = 32000, 1, 0) +
      if(type = 'bb', 1, 0) +
      if(launched = 1915, 1, 0) +
      if(c.class = 'Kongo', 1, 0) +
      if(country = 'USA', 1, 0) >=4