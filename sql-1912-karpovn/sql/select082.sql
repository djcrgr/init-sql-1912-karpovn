WITH res (code, price, number) AS (
 SELECT PC.code,PC.price, ROW_NUMBER() OVER (ORDER BY PC.code) number
FROM PC
)
SELECT res.code, AVG(r.price)
FROM res
JOIN res r ON (r.number-res.number)<6 AND (r.number-res.number)>=0
GROUP BY res.number, res.code
HAVING COUNT(res.number)=6