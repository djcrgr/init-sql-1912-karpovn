SELECT ship as name from (SELECT ship FROM outcomes
UNION
SELECT name as ship from ships) s
WHERE (length(s.ship) - length(replace(s.ship, ' ', ''))) >= 2