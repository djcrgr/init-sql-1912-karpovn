SELECT maker
FROM product
         LEFT JOIN pc p ON product.model = p.model
WHERE product.type = 'pc'
GROUP BY maker, type
HAVING count(product.type) = count(p.model)