SELECT point,
       date,
       CASE
           WHEN
               NOT exists(SELECT point, date
                          FROM outcome_o o
                          WHERE inc.date = o.date
                            AND inc.point = o.point
                            AND dayname(date) <> 'Sunday') THEN date
           ELSE (SELECT min(w.dt)
                 FROM (SELECT q.*
                       FROM (SELECT point,
                                    CASE
                                        WHEN dayname(date) = 'Saturday'
                                            THEN
                                            date_add(date, INTERVAL 2 DAY)
                                        ELSE
                                            date_add(date, INTERVAL 1 DAY)
                                        END AS dt
                             FROM outcome_o) q
                       WHERE NOT exists
                           (SELECT 1
                            FROM outcome_o oo
                            WHERE q.dt = oo.date
                              AND q.point = oo.point)
                      ) w
                 WHERE w.point = inc.point
                   AND w.dt > inc.date
           )
           END d
FROM income_o AS inc