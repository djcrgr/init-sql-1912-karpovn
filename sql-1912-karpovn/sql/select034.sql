select name
from ships s
         join classes c on s.class = c.class
where s.launched >= 1922
  and c.type = 'bb'
  and c.displacement > 35000