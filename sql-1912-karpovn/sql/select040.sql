SELECT maker, type
FROM Product
WHERE maker IN
      (SELECT maker
       FROM (SELECT maker, type FROM Product GROUP BY maker, type) res
       GROUP BY maker
       HAVING count(model) = 1)
GROUP BY maker, type
HAVING count(model) > 1