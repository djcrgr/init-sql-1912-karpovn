WITH w AS (SELECT sum(inc) AS rest
           FROM income_o
           WHERE date < '2001-04-15'
           UNION
           SELECT -sum(`out`)
           FROM outcome_o
           WHERE date < '2001-04-15')
SELECT SUM(rest)
FROM w

