with r as (select v.v_name,
       v.v_id,
       count(case when v_color = 'R' then 1 end) over(partition by v_id) red,
       count(case when v_color = 'B' then 1 end) over(partition by b_q_id) blue
  from utV v join utB b on v.v_id = b.b_v_id)
select v_name
  from r
where red > 1
  and blue > 0
group by v_name