SELECT p.name, count(*) AS res
FROM passenger AS p
         JOIN pass_in_trip AS pt ON pt.id_psg = p.id_psg
         JOIN trip AS t ON t.trip_no = pt.trip_no
WHERE town_to = 'moscow'
  AND p.id_psg NOT IN (
    SELECT p.id_psg
    FROM passenger AS p
             JOIN pass_in_trip AS pt ON pt.id_psg = p.id_psg
             JOIN trip AS t ON t.trip_no = pt.trip_no
    GROUP BY id_psg
    HAVING group_concat(town_from ORDER BY date, time_out ASC SEPARATOR '/') LIKE 'moscow%'
)
GROUP BY p.id_psg
HAVING res > 1