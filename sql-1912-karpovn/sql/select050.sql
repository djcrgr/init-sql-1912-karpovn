SELECT battle
FROM outcomes
         JOIN battles b ON outcomes.battle = b.name
WHERE ship IN (SELECT name FROM ships WHERE ships.class = 'Kongo')