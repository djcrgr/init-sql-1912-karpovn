SELECT maker, GROUP_CONCAT(DISTINCT type ORDER BY type SEPARATOR '/') models
FROM product
GROUP BY maker;