SELECT cast(avg(numguns) as DECIMAL (6,2)) as res
FROM (
         SELECT numguns, name
         FROM classes
                  JOIN ships USING (class)
         WHERE type = 'bb'
           AND name != 'null'
           AND class != 'null'
         UNION ALL
         SELECT DISTINCT numguns, ship
         FROM classes
                  LEFT JOIN outcomes ON classes.class = outcomes.ship
         WHERE ship NOT IN (SELECT name FROM ships)
           AND class != 'null'
           AND type = 'bb') a