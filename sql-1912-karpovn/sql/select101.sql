SELECT code,
       model,
       color,
       type,
       price,
       MAX(model) OVER (PARTITION BY colors)                  max_model,
       MAX(if(type = 'Laser', 1, 0)) OVER (PARTITION BY colors) +
       MAX(if(type = 'Matrix', 1, 0)) OVER (PARTITION BY colors) +
       MAX(if(type = 'Jet', 1, 0)) OVER (PARTITION BY colors) distinct_types,
       AVG(price) OVER (PARTITION BY colors)
FROM (
         SELECT *,
                if(color = 'n', 0, ROW_NUMBER() OVER (ORDER BY code)) +
                if(color = 'n', 1, -1) * ROW_NUMBER() OVER (PARTITION BY color ORDER BY code) colors
         FROM printer
     ) AS res