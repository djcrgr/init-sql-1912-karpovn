SELECT maker
FROM product
GROUP BY maker
HAVING count(DISTINCT type) = 1
   AND (min(type) = 'printer' OR
        (min(type) = 'pc' AND count(model) >= 3));