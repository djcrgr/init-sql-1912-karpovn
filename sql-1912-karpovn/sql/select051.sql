WITH sh AS (
    SELECT name, class
    FROM ships
    UNION
    SELECT ship, ship
    FROM outcomes
)
SELECT name
FROM sh
         JOIN classes c ON sh.class = c.class
WHERE numguns >= ALL (
    SELECT ci.numguns
    FROM classes ci
    WHERE ci.displacement = c.displacement
      AND ci.class IN (SELECT sh.class FROM sh))