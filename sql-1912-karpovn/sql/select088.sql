SELECT (SELECT name FROM passenger WHERE id_psg = t1.id_psg) name,
       t1.count,
       (SELECT name FROM company WHERE id_comp = t1.id_comp) company
FROM (
         SELECT pt.id_psg             AS id_psg,
                count(*)              AS count,
                MIN(t.id_comp)        AS id_comp,
                MAX(COUNT(*)) OVER () AS max_qty
         FROM pass_in_trip pt,
              trip t
         WHERE pt.trip_no = t.trip_no
         GROUP BY pt.id_psg
         HAVING MAX(t.id_comp) = MIN(t.id_comp)) t1
WHERE t1.max_qty = t1.count;