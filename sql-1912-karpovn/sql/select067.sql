SELECT count(town_from)
FROM (SELECT town_from, town_to, count(plane)
      FROM trip
      GROUP BY town_from, town_to
      HAVING count(plane) >= ALL (SELECT count(plane)
                                  FROM trip
                                  GROUP BY town_from, town_to)) AS w