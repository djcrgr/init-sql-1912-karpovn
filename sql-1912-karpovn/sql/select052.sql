SELECT name
FROM ships
         JOIN classes c ON ships.class = c.class
WHERE country = 'Japan'
  AND type = 'bb'
  AND (numguns >= 9 OR numguns IS NULL)
  AND (bore < 19 OR bore IS NULL)
  AND (displacement <= 65000 OR displacement IS NULL)