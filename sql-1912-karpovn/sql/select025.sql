select distinct maker from product
where maker in (SELECT maker FROM product WHERE type='printer'
)
and
model in (
select model from pc
where ram = (select min(ram) from pc)
and
speed = (select max(speed) from pc where ram = (select min(ram) from pc ))
)
