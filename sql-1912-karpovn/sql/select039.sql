select distinct ship
from outcomes o
         join battles b on o.battle = b.name
where ship in (select ship
               from outcomes
                        join battles b2 on outcomes.battle = b2.name
               where b.date < b2.date
                 and o.result = 'damaged')