SELECT name
FROM classes
         JOIN ships s ON classes.class = s.class
WHERE bore = 16
UNION
SELECT ship
FROM outcomes
         JOIN classes ON class = ship
WHERE bore = 16