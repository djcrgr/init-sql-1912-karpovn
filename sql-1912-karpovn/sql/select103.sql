SELECT MIN(a.trip_no),
       MIN(b.trip_no),
       MIN(c.trip_no),
       MAX(a.trip_no),
       MAX(b.trip_no),
       MAX(c.trip_no)
FROM trip a,
     trip b,
     trip c
WHERE a.trip_no < b.trip_no
  AND b.trip_no < c.trip_no