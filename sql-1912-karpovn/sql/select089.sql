WITH res AS (
    SELECT count(model) count
    FROM product
    GROUP BY maker
)
SELECT maker, COUNT(DISTINCT model)
FROM product
GROUP BY maker
HAVING count(model) = (SELECT MAX(count) FROM res)
    OR count(model) = (SELECT MIN(count) FROM res)