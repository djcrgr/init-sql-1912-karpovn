WITH res AS (SELECT name, count_fl
             FROM passenger
                      JOIN
                  (SELECT c1, max(count_fl) count_fl
                   FROM (
                            SELECT pass_in_trip.id_psg c1, trip.id_comp c2, count(*) count_fl
                            FROM pass_in_trip
                                     JOIN trip ON trip.trip_no = pass_in_trip.trip_no
                            GROUP BY pass_in_trip.id_psg, trip.id_comp
                        ) AS t
                   GROUP BY c1
                   HAVING count(*) = 1) AS tt
                  ON id_psg = c1
             ORDER BY count_fl DESC)
SELECT name, count_fl
FROM res
GROUP BY name
HAVING count_fl = (SELECT max(count_fl) FROM res)