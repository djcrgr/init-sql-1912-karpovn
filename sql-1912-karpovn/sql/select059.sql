SELECT c1,
       c2 - (if(o2 IS NULL , 0, o2))
FROM (SELECT point c1, sum(inc) c2
      FROM income_o
      GROUP BY point) AS t1
         LEFT JOIN
     (SELECT point o1, sum(`out`) o2
      FROM outcome_o
      GROUP BY point) AS t2
     ON c1 = o1