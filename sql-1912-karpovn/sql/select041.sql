SELECT DISTINCT p.maker,
                CASE
                    WHEN MAX(IF(d.price IS NULL, NULL, 0)) = 0 THEN
                        MAX(d.price) END AS m_price
FROM product p
         RIGHT JOIN (SELECT model, price
                     FROM pc
                     UNION
                     SELECT model, price
                     FROM laptop
                     UNION
                     SELECT model, price
                     FROM printer) d ON p.model = d.model
GROUP BY p.maker