with res as (SELECT passenger.id_psg, passenger.name, trip.town_from
    FROM passenger,
         pass_in_trip,
         trip
    WHERE pass_in_trip.id_psg = passenger.id_psg
      AND pass_in_trip.trip_no = trip.trip_no
    UNION
    SELECT passenger.id_psg, passenger.name, trip.town_to
    FROM passenger,
         pass_in_trip,
         trip
    WHERE pass_in_trip.id_psg = passenger.id_psg
      AND pass_in_trip.trip_no = trip.trip_no)
SELECT name
FROM res
GROUP BY name
HAVING COUNT(*) = 2;