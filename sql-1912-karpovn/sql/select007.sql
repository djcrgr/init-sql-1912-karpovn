select distinct product.model, pc.price from product join pc
on product.model = pc.model where product.maker = 'B'
union
select distinct product.model, laptop.price from product join laptop
on product.model = laptop.model where product.maker = 'B'
union
select distinct product.model, printer.price from product join printer
on product.model = printer.model where product.maker = 'B'
