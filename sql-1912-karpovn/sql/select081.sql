SELECT code, point, date, `out`
FROM outcome
         JOIN (
    SELECT YEAR(date) y, MONTH(date) m, sum(`out`) total, RANK() OVER (ORDER BY sum(`out`) DESC) rk
    FROM outcome
    GROUP BY y, m
             ORDER BY date, Total
) r ON YEAR(outcome.date) = r.y AND MONTH(outcome.date) = r.m AND rk=1