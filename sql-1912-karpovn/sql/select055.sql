SELECT c.class, t.lanch
FROM classes c
         LEFT JOIN
     (SELECT class, MIN(launched) AS lanch
      FROM ships
      GROUP BY class
     ) AS t ON c.class = t.class
