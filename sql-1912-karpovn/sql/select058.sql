/*Для каждого типа продукции и каждого производителя из таблицы Product c точностью до двух
десятичных знаков найти процентное отношение числа моделей данного типа
данного производителя к общему числу моделей этого производителя.
Вывод: maker, type, процентное отношение числа моделей данного типа к
общему числу моделей производителя*/
SELECT m, t,
CAST(100.0*cc/cc1 AS DECIMAL (5,2))
from
(SELECT m, t, sum(c) cc from
(SELECT distinct maker m, 'PC' t, 0 c from product
union all
SELECT distinct maker, 'Laptop', 0 from product
union all
SELECT distinct maker, 'Printer', 0 from product
union all
SELECT maker, type, count(*) from product
group by maker, type) as res
group by m, t) res1
JOIN (
SELECT maker, count(*) cc1 from product group by maker
) res2
ON m=maker