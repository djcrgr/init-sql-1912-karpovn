SELECT DISTINCT p.model, l.model, p.speed, p.ram
FROM pc p, pc l
WHERE p.speed = l.speed AND p.ram = l.ram AND p.model > l.model
