SELECT CAST(SUM(vol)/(SELECT COUNT(Q_ID) FROM utq) AS DECIMAL(6, 2))
FROM (
         SELECT if(SUM(b_vol) IS NULL, 0, SUM(b_vol)) vol, b_q_id
         FROM utq LEFT JOIN utb ub ON utq.q_id = ub.b_q_id
         GROUP BY b_q_id
     ) w