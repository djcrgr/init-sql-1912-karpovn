/*Укажите сражения, которые произошли в годы, не совпадающие ни с одним из годов спуска кораблей на воду.*/
SELECT b.name
FROM battles b
WHERE YEAR(b.date) NOT IN (SELECT launched FROM ships WHERE launched IS NOT NULL )