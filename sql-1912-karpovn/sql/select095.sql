SELECT c.name                              AS company_name,
       count(DISTINCT w.trip_no, w.date) AS flights,
       count(DISTINCT t.plane)             AS planes,
       count(DISTINCT w.id_psg)           AS diff_psngrs,
       count(*)                            AS total_psngrs
FROM pass_in_trip AS w
         JOIN trip AS t ON t.trip_no = w.trip_no
         JOIN company AS c ON c.id_comp = t.id_comp
GROUP BY c.id_comp
ORDER BY company_name