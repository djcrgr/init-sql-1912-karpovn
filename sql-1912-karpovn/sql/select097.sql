SELECT code, a, b, c, d
FROM (SELECT code, speed AS a, ram AS b, price AS c, screen AS d FROM laptop) a
WHERE power(10, if(b >= 2 * a, 1, 0) + if(c >= 2 * a, 1, 0) + if(d >= 2 * a, 1, 0)) +
      power(10, if(a >= 2 * b, 1, 0) + if(c >= 2 * b, 1, 0) + if(d >= 2 * b, 1, 0)) +
      power(10, if(a >= 2 * c, 1, 0) + if(b >= 2 * c, 1, 0) + if(d >= 2 * c, 1, 0)) +
      power(10, if(a >= 2 * d, 1, 0) + if(b >= 2 * d, 1, 0) + if(c >= 2 * d, 1, 0)) = 1111