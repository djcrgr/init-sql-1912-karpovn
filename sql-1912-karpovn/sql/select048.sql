SELECT c.class
FROM classes c
         LEFT JOIN ships s ON s.class = c.class
WHERE c.class IN (SELECT ship FROM outcomes WHERE result = 'sunk')
   OR s.name IN (SELECT ship FROM outcomes WHERE result = 'sunk')
GROUP BY c.class
