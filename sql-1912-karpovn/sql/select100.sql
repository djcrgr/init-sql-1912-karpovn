SELECT date.date, date.num, inc.point, inc.inc, `out`.point, `out`.out

FROM (
         SELECT date, row_number() OVER (PARTITION BY date ORDER BY code ) AS num
         FROM income
         UNION
         SELECT date, row_number() OVER (PARTITION BY date ORDER BY code ) AS num
         FROM outcome) date
         LEFT JOIN
     (
         SELECT date, row_number() OVER (PARTITION BY date ORDER BY code ) AS num, point, inc
         FROM income) AS inc
     ON date.date = inc.date AND date.num = inc.num
         LEFT JOIN
     (
         SELECT date, row_number() OVER (PARTITION BY date ORDER BY code ) AS num, point, `out`
         FROM outcome) AS `out`
     ON date.date = out.date AND date.num = out.num