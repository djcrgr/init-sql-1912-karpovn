INSERT INTO PC (code, model, speed, ram, hd, cd, price)
SELECT MIN(code) + 20,
       model + 1000,
       MAX(speed),
       MAX(ram) * 2,
       MAX(hd) * 2,
       (SELECT CONCAT(CAST((MAX(CAST(REPLACE(cd, 'x', '') AS DECIMAL))) AS CHAR(2)), 'x') FROM PC),
       MAX(price) / 1.5
FROM Laptop
GROUP BY model