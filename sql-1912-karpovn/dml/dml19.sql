INSERT INTO Outcomes (ship, battle, result)
SELECT o.ship, b.name, 'sunk'
FROM Outcomes o
         JOIN Battles a ON o.battle = a.name
         JOIN Battles b ON (
                                SELECT MIN(B3.date)
                                FROM Battles B3
                                WHERE B3.date > a.date
                            ) = b.date
WHERE o.ship IN (
    SELECT ship
    FROM Outcomes
    WHERE result = 'damaged'
)
  AND o.ship IN (
    SELECT ship
    FROM Outcomes
    GROUP BY ship
    HAVING COUNT(ship) = 1
);