DELETE
FROM Classes
WHERE class NOT IN
      (SELECT w.class
       FROM (
                SELECT class, COUNT(class) count
                FROM Ships
                GROUP BY class
                UNION ALL
                SELECT ship class, count(ship) count
                FROM Outcomes
                WHERE ship NOT IN (
                    SELECT name
                    FROM Ships
                )
                GROUP BY ship
            ) w
       GROUP BY w.class
       HAVING SUM(w.count) >= 3
      );
