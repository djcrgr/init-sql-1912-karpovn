INSERT INTO PC (code, model, speed, ram, hd, cd, price)
SELECT model + (SELECT MAX(code) FROM PC),
       model,
       (SELECT MAX(speed) FROM PC),
       (SELECT MAX(ram) FROM PC),
       (SELECT MAX(hd) FROM PC),
       (SELECT CONCAT(CAST((MAX(CAST(REPLACE(cd, 'x', '') AS DECIMAL))) AS VARCHAR(3)), 'x') FROM PC),
       (SELECT AVG(price) FROM PC)
FROM Product
WHERE type = 'PC'
  AND model not IN (SELECT model FROM PC)