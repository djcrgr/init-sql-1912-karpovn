DELETE
FROM PC
WHERE code NOT IN (
    SELECT MAX(code)
    FROM PC
    GROUP BY model
);