UPDATE Laptop
SET screen=screen + 1,
    price=price - 100
WHERE model IN (SELECT model FROM Product where maker IN ('E', 'B'));
