DELETE
FROM Ships
WHERE name IN (
    SELECT ship name
    FROM Outcomes
    WHERE result = 'sunk'
);