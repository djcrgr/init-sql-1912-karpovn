DELETE
FROM Product
WHERE model NOT IN (
    SELECT model
    FROM Printer
    UNION
    SELECT model
    FROM Laptop
    UNION
    SELECT model
    FROM PC
);